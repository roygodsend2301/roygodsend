from django import forms

from .models import PostModel

class PostForm(forms.ModelForm):
  class Meta:
    model = PostModel
    fields = [
      'namaKegiatan',
      'hari',
      'tanggal',
      'waktu',
    ]

    widgets = {
      'namaKegiatan' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'isi dengan judul kegiatan'
                }
            ),
      'hari' : forms.Select (
                attrs = {
                    'class' : 'form-control',
                }
            ),
      'tanggal' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'type' : 'date',
                }
            ),

      'waktu' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'type' : 'time',
                }
            ),      
    }