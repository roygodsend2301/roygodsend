from django.urls import path, re_path
from . import views

urlpatterns = [
  path('',views.home),
  path('about/',views.about),
  path('experience/', views.experience),
  path('contact/',views.contact),
  path('jadwal/', views.jadwal, name = "jadwal"),
  path('create/', views.create, name ="createJadwal"),
  re_path('deletekegiatan/(?P<delete_id>.*)/', views.delete,name="delete")
]