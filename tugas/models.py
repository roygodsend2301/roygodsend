from django.db import models

class PostModel(models.Model):
    namaKegiatan = models.CharField(max_length = 255)
    DAYS = (
      ('Senin', 'Senin'),
      ('Selasa', 'Selasa'),
      ('Rabu', 'Rabu'),
      ('Kamis', 'Kamis'),
      ('Jumat', 'Jumat'),
      ('Sabtu', 'Sabtu'),
      ('Minggu', 'Minggu'),
    )

    hari = models.CharField(max_length = 100, choices = DAYS, default = 'Senin')
    tanggal = models.DateField(auto_now=False, auto_now_add=False)
    waktu = models.TimeField(auto_now=False, auto_now_add=False)

    def __str__(self):
      return "{}".format(self.namaKegiatan)