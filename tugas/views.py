from django.shortcuts import render,redirect

from .forms import PostForm
from .models import PostModel 

def home(request):
  return render(request, 'pages/index.html')

def about(request):
  return render(request, 'pages/aboutMe.html')

def experience(request):
  return render(request, 'pages/experience.html')

def contact(request):
  return render(request, 'pages/contact.html')
  
def jadwal(request):
  post = PostModel.objects.all()

  context = {
    'posts':post,
  }
  return render(request, 'pages/jadwal.html',context)

def create (request):
  post_form = PostForm(request.POST or None)

  if request.method == 'POST' :
    if post_form.is_valid():
      post_form.save()
      return redirect('jadwal')

  context = {
    'page_title' : 'Create Post',
    'post_form' : post_form
  }

  return render(request, 'pages/create.html',context)

def delete(request,delete_id):
  PostModel.objects.filter(id = delete_id).delete()

  return redirect('jadwal')
